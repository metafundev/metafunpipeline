library (metafor)
library (rjson)

(args = commandArgs(trailingOnly = TRUE))
(dataDir = paste0(args [1],"/metaRes/meta_analysis.Rda"))
GO = args [2]
(fileName = paste0(args[1],"/metaRes/GOs/",GO,".txt"))
load(dataDir)
dataGO = metaResult[[GO]]

cilb = dataGO$yi - qnorm(dataGO$level/2, lower.tail=FALSE) * sqrt(dataGO$vi)
ciub = dataGO$yi + qnorm(dataGO$level/2, lower.tail=FALSE) * sqrt(dataGO$vi)
weigth = weights(dataGO)
lor = dataGO$yi
se = dataGo$vi
forestData = data.frame(cilb, lor, ciub, weigth, vi)
varToJSON = split (forestData, 1:nrow(forestData))
names(varToJSON) = row.names(forestData)
jsonGO = toJSON(varToJSON)
write(jsonGO, file = fileName)
