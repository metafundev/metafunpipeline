#!/usr/bin/env Rscript
library(Biobase)
library(limma)
library(ggplot2)
library(metafor)
library(plyr)
library(mdgsa)
#library(hipathia)
library(ggdendro)
library(rjson)
library(GO.db)
library(mongolite)
library(org.Hs.eg.db)
library(org.Mm.eg.db)
library(org.Rn.eg.db)

doMetaFun <- function(args) {
    for (i in 1:length(data[[1]])) {
        expressiondata <- read.csv(as.character(data$exprFiles[i]),
                                                header = TRUE)
        rownames(expressiondata) <- expressiondata[[1]]
        expressiondata[1] <- NULL
        designData <- read.table(as.character(data$tsvFiles[i]))
        print (i)
        (experimentDesign <- as.factor(designData[[2]]))
        sample.names <- designData[[1]]
        if (ops[4] == "-CS") {
            experimentDesign <- mapvalues(experimentDesign,
                                          from = design.values[1:2],
                                          to = c("Case", "Control"))
            contrast <- "Case-Control"

        } else {
            experimentDesign <- mapvalues(experimentDesign,
                                          from = design.values[1:4],
                                          to = c("ME", "MS", "HE", "HS"))
            contrast <- "(ME-MS)-(HE-HS)"
            print(contrast)
        }
        write(contrast, paste0(writingDirectory,"/jobDesign.txt"))
        partialSummary <- cbind(designData, experimentDesign, stdNames[i, 2])
        summaryDDF <- rbind(summaryDDF, partialSummary)

        #PCA

        (writedir <- paste0(writingDirectory, "/pca", i, ".txt"))
        pcas <- pcaGenes(expressiondata)
        pca.df <- as.data.frame(pcas$scores)
        pca.df$Design <- experimentDesign
        pca.df$var.exp <- pcas$var.exp
        rownames(pca.df) <- sample.names
        if (ops[4] == "-CS") {
            caseControl <- c(which(pca.df$Design == "Control"),
                             which(pca.df$Design == "Case"))
        } else {
            caseControl <- c(which(pca.df$Design == "ME"),
                             which(pca.df$Design == "MS"),
                             which(pca.df$Design == "HS"),
                             which(pca.df$Design == "HE"))
        }
        pca.newDf <- pca.df[caseControl]
        pcaPlot <- plotPca(pcaDf = pca.newDf, condition = experimentDesign)
        summaryPcaPlot <- data.frame(pca.df$V1,
                                     pca.df$V2,
                                     pca.df$Design,
                                     row.names = rownames(pca.df))
        colnames(summaryPcaPlot) <- c("PC1", "PC2", "Design")
        write.table(summaryPcaPlot,
                    file = writedir,
                    row.names = TRUE,
                    col.names = FALSE,
                    quote = FALSE,
                    sep = "\t")
        designData$V2 <- pca.df$Design

        #BOXPLOT
        writedir <- paste0(writingDirectory, "/boxplot", i, ".txt")
        names <- rep(sample.names, each = dim(expressiondata)[1])
        values <- c()
        for (col in colnames(expressiondata)) {
        values <- c(values, expressiondata[, col])
        }
        cond <- rep(experimentDesign, each = dim(expressiondata)[1])
        box_data <- data.frame(names, values, cond)
        (boxPlot <- plotBoxplot(box_data, names, values, cond))
        numSamples <- length(levels(box_data$names))
        valuesPerSample <- length(box_data$values) / numSamples

        summaryBoxplot <- data.frame(sampleName = character(),
                                     minS = numeric(),
                                     Q1 = numeric(),
                                     Q2 = numeric(),
                                     Q3 = numeric(),
                                     maxS = numeric(),
                                     design = character())
        for (j in 1:numSamples) {
            sampleIni <- (j * valuesPerSample) - valuesPerSample + 1
            sampleFin <- j * valuesPerSample
            sampleName <- as.character(box_data$names[sampleIni])
            minS <- as.numeric(min(box_data$values[sampleIni:sampleFin]))
            Q1 <- as.numeric(quantile(box_data$values[sampleIni:sampleFin], 0.25))
            Q2 <- as.numeric(quantile(box_data$values[sampleIni:sampleFin], 0.5))
            Q3 <- as.numeric(quantile(box_data$values[sampleIni:sampleFin], 0.75))
            maxS <- as.numeric(max(box_data$values[sampleIni:sampleFin]))
            des <- pca.df$Design[j]
            row <- data.frame (sampleName, minS, Q1, Q2, Q3, maxS, des)
            summaryBoxplot <- rbind(summaryBoxplot, row)
        }

        colnames(summaryBoxplot) <- c("sampleName",
                                      "minS",
                                      "Q1",
                                      "Q2",
                                      "Q3",
                                      "maxS",
                                      "design")
        write.table(summaryBoxplot,
                    file=writedir,
                    col.names = FALSE,
                    row.names = FALSE,
                    quote = FALSE,
                    sep = "\t")

        #CLUSTERING
        writedir <- paste0(writingDirectory, "/clustering", i, ".png")
        correlation <- cor(expressiondata)
        distance <- as.dist((1 - correlation) / 2)
        hc <- hclust(distance)
        legendClass <- factor(experimentDesign,
                              levels = c("ME", "MS", "HE", "HS"))
        levels(legendClass) <- c("Case Female", "Control Female",
                                 "Case Male", "Control Male")
        hc$clase <- legendClass
        clusterPlot <- plotTreeClust(cluster = hc,
                                      title = "Sample Clustering")
        ggsave(writedir, plot = clusterPlot)

        #EXPRESSION DIFERENCIAL
        writedir <- paste0(writingDirectory, "/difExp", i, ".txt")

        if (data[i,3] != "MicroArray") {
            diffexpression <- diffExp(expressiondata,
                                      experimentDesign,
                                      contrast,
                                      trend = TRUE)
        } else {
            diffexpression <- diffExp(expressiondata,
                                      experimentDesign,
                                      contrast)
        }
        GeneName <- select(org.Hs.eg.db, rownames(diffexpression),
                                         columns = "GENENAME",
                                         keytype = "ENTREZID")[2]
        diffexpression$GeneName <- GeneName
        write.table(diffexpression, as.character(writedir))
        diffexpTopTable <- topTable(diffexpression,
                                number = length(diffexpression$coefficients))
        topTable50 <- topTable(diffexpression, number = 50)
        GeneName <- select(org.Hs.eg.db, rownames(topTable50),
                            columns = "GENENAME", keytype = "ENTREZID")[2]
        topTable50 <- cbind(topTable50, rownames(topTable50), GeneName)
        colnames(topTable50) <- c("logFC", "AveExpr",
                                    "t", "P.Value",
                                    "adj.P.Val", "B",
                                    "Entrez", "GeneName")
        write.table(topTable50, 
                    paste0(writingDirectory, "/difExpTop", i, ".txt"),
                    sep = "\t",
                    row.names = FALSE,
                    col.names = TRUE)

        summaryDifExp$Total.genes[i] <- length(diffexpTopTable$logFC)
        indices <- diffexpTopTable$adj.P.Val < 0.05
        summaryDifExp$sig.Total[i] <- sum(indices)
        summaryDifExp$sig.UP[i] <- sum(diffexpTopTable$logFC[indices ] >0)
        summaryDifExp$sig.DOWN[i] <- sum(diffexpTopTable$logFC[indices] < 0)

        ##GSEA
        if (ops[2] == "-GSA") {
            entrez <- rownames(diffexpression)
            if (ops[3] == "-HS") {
            goids <- select(org.Hs.eg.db, entrez,
                            columns = c("ENTREZID", "GO"), keytype = "ENTREZID")
            } else if (ops[3] == "-MM") {
            goids <- select(org.Mm.eg.db, entrez,
                            columns = c("ENTREZID", "GO"), keytype = "ENTREZID")
            } else if (ops[3] == "-RN") {
            goids <- select(org.Rn.eg.db, entrez,
                            columns = c("ENTREZID", "GO"), keytype = "ENTREZID")
            }
            colnames(goids) <- c("ENTREZID", "GO")
            #gseaObj = doGSEA(diffexpression, goids, "bp", propagate = TRUE, minBlockSize = 1, maxBlockSize = 50000)##########FRANC#############
            gseaObj <- doGSEA(diffexpression,
                              goids,
                              "bp",
                              propagate = FALSE)
            gseaObj <- cbind(gseaObj, rownames(gseaObj))
            colnames(gseaObj) <- c("N", "lor",
                                    "pval", "adjPval",
                                    "sd", "t",
                                    "conv", "GOterm")
            gseaObj$GOName <- Term(GOTERM[rownames(gseaObj)])

            write.table(gseaObj,
                        paste0(writingDirectory, "/gsea", i, ".txt"),
                        sep = "\t",
                        row.names = FALSE,
                        col.names = TRUE)
            gseaTop <- gseaObj[order(gseaObj$adjPval),]
            write.table(gseaTop[1:50, ], paste0(writingDirectory,
                        "/topGsea", i, ".txt"),
                        sep = "\t",
                        row.names = FALSE,
                        col.names = TRUE)

            summaryGSEA$Total.functions[i] <- length(gseaObj$lor)
            indices <- gseaObj$adjPval < 0.05
            summaryGSEA$sig.Total[i] <- sum(indices)
            summaryGSEA$sig.UP[i] <- sum(gseaObj$lor[indices] > 0)
            summaryGSEA$sig.DOWN[i] <- sum(gseaObj$lor[indices] < 0)
        } else if (ops[2] == "-Hipathia") {
            if (ops[3] == "-HS") {
            iPathways <- hipathia::load_pathways("hsa")
            } else if (ops[3] == "-MM") {
            iPathways <- hipathia::load_pathways("mmu")
            } else if (ops[3] == "-RN") {
            iPathways <- hipathia::load_pathways("rno")
            }
            ##TODO: hipathia analisis
            muestras <- designData
            rownames(muestras) <- muestras[, 1]
            muestras[, 1] <- NULL
            colnames(muestras) <- "Condition"
            metadatos <- data.frame(labelDescription = c("Condition"),
                                     row.names = "Condition")
            datosfenotipo <- new("AnnotatedDataFrame",
                                 data = muestras,
                                 varMetadata = metadatos)
            datosexperimento <- new("MIAME", name = "hipathiaMI")
            exprSet <- new("ExpressionSet",
                           exprs = as.matrix(expressiondata),
                           phenoData = datosfenotipo)
            hipathRes <- doHipathia(exprSet, iPathways)
            pathwaysRes <- doPathways(hipathRes,
                                      pathways = iPathways,
                                      contrast = contrast,
                                      paired = FALSE) #TODO: repasar contrast
            orderedRes <- limmaToHipathia(pathwaysRes, 1, iPathways)
            topathia <- orderedRes [order(orderedRes$FDRp.value), ]
            write.table(pathwaysRes,
                        paste0(writingDirectory,
                        "/hipathia", i, ".txt"),
                        sep = "\t", row.names = TRUE, col.names = TRUE)
            write.table(topathia[1:30, ],
                        paste0(writingDirectory,
                        "/topHipathia", i, ".txt"),
                        sep = "\t", row.names = TRUE, col.names = TRUE)

            summaryGSEA$Total.functions[i] <- length(orderedRes$statistic)
            indices <- orderedRes$FDRp.value < 0.05
            summaryGSEA$sig.Total[i] <- sum(indices)
            summaryGSEA$sig.UP[i] <- sum(orderedRes$statistic[indices] > 0)
            summaryGSEA$sig.DOWN[i] <- sum(orderedRes$statistic[indices] < 0)
            }
    }



      printDDF <- data.frame()
      stdTotal <- c()
      for (stdn in stdNames[, 2]) {
      tagT <- c()
      index <- summaryDDF$stdName == stdn
      for (tagN in levels(summaryDDF[[2]])) {
          tagT <- c(tagT, sum(summaryDDF[[2]][index] == tagN))
      }
      tagT <- c(tagT, sum(tagT))
      printDDF <- rbind(printDDF, tagT)
      }
      colnames(printDDF) <- c(levels(summaryDDF[[2]]), "Total")
      rownames(printDDF) <- as.character(stdNames[, 2])
      write.csv(printDDF,
                paste0(writingDirectory, "/summaryDesign.txt"),
                quote = FALSE,
                row.names = TRUE)
      write.csv(summaryGSEA,
                paste0(writingDirectory, "/summaryFunctionalProfiling.txt"),
                quote = FALSE,
                row.names = TRUE)
      write.csv(summaryDifExp,
                paste0(writingDirectory, "/summaryDifExp.txt"),
                quote = FALSE,
                row.names = TRUE)



      #METAANALISIS
      if (ops [2] == "-GSA") {
        gseaFiles <- dir(writingDirectory, pattern = "gsea")
        gos <- NULL
        for (f in gseaFiles) {
          mat <- read.table(paste0(writingDirectory, "/", f), header = TRUE)
          rownames(mat) <- mat$GOterm
          gos <- c(gos, rownames(mat))
        }
        gos <- unique(sort(gos))
        matLOR <- matrix (NA, nrow = length(gos), ncol = length(exprFiles))
        matSD <- matrix (NA, nrow = length(gos), ncol = length(exprFiles))
        rownames(matLOR) <- gos
        rownames(matSD) <- gos
        colnames(matLOR) <- strsplit(basename(exprFiles), ".csv")
        colnames(matSD) <- strsplit(basename(exprFiles), ".csv")
        i = 1
        for (f in gseaFiles) {
          mat <- read.table(paste0(writingDirectory, "/", f), header = TRUE)
          lor <- mat$lor
          sd <- mat$sd
          col <- strsplit(basename(exprFiles[i]), ".csv")
          names(lor) <- mat$GOterm
          names(sd) <- mat$GOterm
          matLOR[,col[[1]]] <- lor[rownames(matLOR)]
          matSD[,col[[1]]] <- sd[rownames(matSD)]
          i <- i + 1
        }

        naSD <- !is.na(matSD)
        ind1 <- apply(naSD, 1, sum)
        keep <- ind1 >= 2 #(length(gseaFiles) -1)

        matSD <- matSD[keep,]
        matLOR <- matLOR[keep,]

        write.table(matLOR,
                    file = paste0(writingDirectory, "/metaRes/matLOR.txt"),
                    quote = FALSE, sep = "\t",
                    row.names = TRUE, col.names = TRUE)
        write.table(matSD,
                    file = paste0(writingDirectory, "/metaRes/matSD.txt"),
                    quote = FALSE, sep = "\t",
                    row.names = TRUE, col.names = TRUE)
        metaResult <- NULL
        if (ops[1] == "-FixedEffect") {
          metaResult <- metaGO(mat.lor=matLOR, mat.sd = matSD,
                               methods = "FE",
                               res.path = paste0(writingDirectory,"/metaRes"),
                               alpha = 0.05, OR.threshold = 0.5,
                               adj.method = "fdr")
          metaType <- "FE"
        } else if (ops[1] == "-RandomEffect") {
          metaResult <- metaGO(mat.lor=matLOR, mat.sd = matSD,
                               methods = "DL",
                               res.path = paste0(writingDirectory,"/metaRes"),
                               alpha = 0.05, OR.threshold = 0.5,
                               adj.method = "fdr")
          metaType <- "DL"
        }
        save(metaResult, file = paste0(writingDirectory,
                                        "/metaRes/meta_analysis.Rda"))

        if (file.exists(paste0(writingDirectory,
                          "/metaRes/sig.results.",
                          metaType, ".txt"))) {
          sigGos <- read.delim(paste0(writingDirectory,
                              "/metaRes/sig.results.",
                              metaType, ".txt"),
                               header = TRUE)

          summaryMeta <- data.frame(row.names = c("BP"))
          summaryMeta$Total.functions <- length(metaResult)
          summaryMeta$sig.UP <- sum(sigGos$summary_LOR > 0)
          summaryMeta$sig.DOWN <- sum(sigGos$summary_LOR < 0)
          summaryMeta$sig.Total <- (summaryMeta$sig.UP + summaryMeta$sig.DOWN)
          write.csv(summaryMeta,
                    paste0(writingDirectory, "/summaryMetaanalysis.txt"),
                    quote = FALSE, row.names = TRUE)
        } else {
          sigGos <- read.delim(paste0(writingDirectory, "/metaRes/all.results.",
                                      metaType, ".txt"), header = TRUE)
          #TODO: sort and get only the 20 first elements
        }
        sigGos <- as.character(sigGos$ID)
        for (go in sigGos) {
          filename <- paste0(writingDirectory, "/metaRes/GOs/", go, ".txt")
          dataGO <- metaResult[[go]]
          cilb <- dataGO$yi - qnorm(dataGO$level/2,
                                    lower.tail = FALSE) * sqrt(dataGO$vi)
          ciub <- dataGO$yi + qnorm(dataGO$level/2,
                                    lower.tail = FALSE) * sqrt(dataGO$vi)
          weight <- weights(dataGO)
          lor <- dataGO$yi
          se <- sqrt(dataGO$vi)
          forestData <- data.frame(cilb, lor, ciub, weight, se)
          varToJSON <- split (forestData, 1 : nrow(forestData))
          names(varToJSON) <- row.names(forestData)
          jsonGO <- toJSON(varToJSON)
          write.table(jsonGO, file = filename,
                      row.names = FALSE, col.names = FALSE)
        }
      }

      if (ops[2] == "-Hipathia") {
        hipathiaFiles = dir(writingDirectory, pattern = "hipathia")
        rutas = NULL
        for (f in hipathiaFiles) {
          mat = read.table(paste0(writingDirectory,"/",f), header = TRUE)
          rutas = c(rutas, rownames(mat))
        }
        rutas = unique(sort(rutas))
        matLOR = matrix (NA, nrow = length(rutas), ncol = length(exprFiles))
        matSD = matrix (NA, nrow = length(rutas), ncol = length(exprFiles))
        rownames(matLOR) = rutas
        rownames(matSD) = rutas
        colnames(matLOR) = strsplit(basename(exprFiles), ".csv")
        colnames(matSD) = strsplit(basename(exprFiles), ".csv")
        i = 1
        for (f in hipathiaFiles) {
          mat = read.table(paste0(writingDirectory,"/",f), header = TRUE)
          lor = mat$coefficients
          sd = sqrt(mat$s2.post) * mat$stdev.unscaled
          col = strsplit(basename(exprFiles[i]), ".csv")
          names(lor) = rownames(mat)
          names(sd) = rownames(mat)
          matLOR[,col[[1]]] = lor[rownames(matLOR)]
          matSD[,col[[1]]] = sd[rownames(matSD)]
          i = i+1
        }
        
        naSD = !is.na(matSD)
        ind1 = apply(naSD, 1, sum)
        keep = ind1 >= 2 #(length(gseaFiles) -1)
        
        matSD = matSD[keep,] 
        matLOR = matLOR[keep,]
        
        write.table(matLOR, file = paste0(writingDirectory,"/metaRes/matLOR.txt"),quote = FALSE, sep = "\t", row.names = TRUE, col.names = TRUE)
        write.table(matSD, file = paste0(writingDirectory,"/metaRes/matSD.txt"),quote = FALSE, sep = "\t", row.names = TRUE, col.names = TRUE)
        metaResult = NULL
        if (ops[1] == "-FixedEffect") {
          metaResult = metaHipathia(mat.lor=matLOR, mat.sd = matSD, methods = "FE", res.path = paste0(writingDirectory,"/metaRes"), alpha = 0.05, OR.threshold = 0.5, adj.method = "fdr", metaginfo="hsa")
          metaType = "FE"
        } else if (ops[1] == "-RandomEffect") {
          metaResult = metaHipathia(mat.lor=matLOR, mat.sd = matSD, methods = "DL", res.path = paste0(writingDirectory,"/metaRes"), alpha = 0.05, OR.threshold = 0.5, adj.method = "fdr", metaginfo=iPathways)
          metaType = "DL"
        }
        save (metaResult, file = paste0(writingDirectory,"/metaRes/meta_analysis.Rda"))
        
        if (file.exists(paste0(writingDirectory,"/metaRes/sig.results.",metaType,".txt")))
        {
          sigPaths = read.delim(paste0(writingDirectory, "/metaRes/sig.results.",metaType,".txt"), header = TRUE)
        } else {
          sigPaths = read.delim(paste0(writingDirectory, "/metaRes/all.results.",metaType,".txt"), header = TRUE)
          #TODO: sort and get only the 20 first elements
        }
        sigPaths = as.character(sigPaths$ID)
        for (pathway in sigPaths) {
          filename = paste0(writingDirectory, "/metaRes/GOs/", pathway, ".txt")
          dataGO = metaResult[[pathway]]
          cilb = dataGO$yi - qnorm(dataGO$level/2, lower.tail=FALSE) * sqrt(dataGO$vi)
          ciub = dataGO$yi + qnorm(dataGO$level/2, lower.tail=FALSE) * sqrt(dataGO$vi)
          weight = weights(dataGO)
          lor = dataGO$yi
          se = sqrt(dataGO$vi)
          forestData = data.frame(cilb, lor, ciub, weight, se)
          varToJSON = split (forestData, 1:nrow(forestData))
          names(varToJSON) = row.names(forestData)
          jsonGO = toJSON(varToJSON)
          write.table(jsonGO, file = filename, row.names = FALSE, col.names = FALSE)
        }
      }
    

    jobName <- strsplit(args[7], "/")
    jobName <- jobName[[1]][length(jobName[[1]])]
    querySel <- paste0('{"name":"', jobName, '"}')
    queryUpd <- paste0('{"$set": {"status": 1}}')
    m <- mongo (url = "mongodb://localhost:27017",
               db = "stevia-server", collection = "jobs")
    m$update(querySel, queryUpd)
}


#args = "-RandomEffect -GSA -HS -GENDER /home/pmalmierca/.metafunusers/poiu/files/Adenocarcinoma/gse75037.csv#MicroArray@/home/pmalmierca/.metafunusers/poiu/files/Adenocarcinoma/gse10072.csv#MicroArray@/home/pmalmierca/.metafunusers/poiu/files/Adenocarcinoma/gse19188.csv#MicroArray@/home/pmalmierca/.metafunusers/poiu/files/Adenocarcinoma/gse32863.csv#MicroArray@ /home/pmalmierca/.metafunusers/poiu/files/SexDifDesign/gse75037des_sex.tsv@/home/pmalmierca/.metafunusers/poiu/files/SexDifDesign/gse10072des_sex.tsv@/home/pmalmierca/.metafunusers/poiu/files/SexDifDesign/gse19188des_sex.tsv@/home/pmalmierca/.metafunusers/poiu/files/SexDifDesign/gse32863des_sex.tsv@ Adenocarcinoma_Female@Control_Female@Adenocarcinoma_Male@Control_Male@Adenocarcinoma_Female@Control_Female@Adenocarcinoma_Male@Control_Male@Adenocarcinoma_Female@Control_Female@Adenocarcinoma_Male@Control_Male@Adenocarcinoma_Female@Control_Female@Control_Male@Control_Male@/home/pmalmierca/.metafunusers/poiu/results/Blablabla > log.txt"
#args = sapply(strsplit(args, " "), function(x) x[])

setwd("/home/pmalmierca/jobsRPipes/")
metafunr <- list.files("/home/pmalmierca/MetaFunR/metafunr/R/")
for (variable in metafunr) {
  source(paste0("/home/pmalmierca/MetaFunR/metafunr/R/", variable),
         local = TRUE)
}
source("function_pcaGenes_2.r", local = TRUE)
args <- commandArgs(trailingOnly = TRUE)

ops <- args[1:4]
files <- args[5:length(args)]

files <- strsplit(files, "@")
csvFiles <- files[[1]]
tsvFiles <- files [[2]]
expDesignOps <- files [[3]]
exprFiles <- c()
tech <- c()
ensembl <- NULL
iPathways <- NULL


exprFiles <- sapply(strsplit(csvFiles, "#"), function(x) x[1])
tech <- sapply(strsplit(csvFiles, "#"), function(x) x[2])

data <- data.frame(exprFiles, tsvFiles, tech)

writingDirectory <- expDesignOps[length(expDesignOps)]
design.values <- expDesignOps[1 : length(expDesignOps) - 1]
stdNames <- data.frame(1:length(exprFiles),
                       tools::file_path_sans_ext(basename(exprFiles[])))
ddfNames <- data.frame(1:length(tsvFiles),
                       tools::file_path_sans_ext(basename(tsvFiles[])))
summaryJobs <- data.frame(exprFiles, tsvFiles, tech)
summaryDifExp <- data.frame(row.names = as.character(stdNames[[2]]))
summaryGSEA <- data.frame(row.names = as.character(stdNames[[2]]))
summaryDDF <- data.frame()
matLOR <- NULL
matSD <- NULL

write.table(stdNames,
            paste0(writingDirectory, "/stdNames.txt"),
            sep = "\t",
            col.names = FALSE,
            row.names = FALSE,
            quote = FALSE)

tryCatch(doMetaFun(args), error = function(e) {
    jobName <- strsplit(args[7], "/")
    jobName <- jobName[[1]][length(jobName[[1]])]
    querySel <- paste0('{"name":"', jobName, '"}')
    queryUpd <- paste0('{"$set": {"status": 2}}')
    m <- mongo(url = "mongodb://localhost:27017",
               db = "stevia-server", collection = "jobs")
    m$update(querySel, queryUpd)
    message(e)
}, finally = {
    message("\nFinished")
})